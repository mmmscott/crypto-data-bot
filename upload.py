import sys
import os
import boto3
import tempfile
from datetime import datetime,timedelta

pairs = ["btc_usd", "btc_usdt", "eth_usd", "eth_usdt", "ltc_usd", "ltc_usdt", "usdt_usd"]

temp_path = tempfile.gettempdir()
yesterday = datetime.now() + timedelta(days=-1)
yesterday = yesterday.strftime('%B-%d-%Y')

files = [yesterday + "-" + pair + ".csv" for pair in pairs]

for file in files:
    to_upload = os.path.join(temp_path, file)

    if os.path.exists(to_upload):
        # Initialize a session using DigitalOcean Spaces.
        session = boto3.session.Session()
        client = session.client('s3',
                            region_name='nyc3',
                            endpoint_url='https://nyc3.digitaloceanspaces.com',
                            aws_access_key_id=os.environ['CRYPTO_ACCESS'],
                            aws_secret_access_key=os.environ['CRYPTO_SECRET'])

        client.upload_file(to_upload, "crypto-data-dumper", file,
                       ExtraArgs={'ContentType': "text/csv", 'ACL': "public-read"})

        os.remove(to_upload)

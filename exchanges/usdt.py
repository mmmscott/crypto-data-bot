from urllib.request import urlopen
import json

poloniex_endpoint = 'https://poloniex.com/public?command=returnTicker'

def get_all_usdt_data(crypto_pairs):
    handle_poloneix(crypto_pairs)
    handle_hitbtc(crypto_pairs)
    handle_bittrex(crypto_pairs)

def handle_poloneix(crypto_pairs):
    value = urlopen(poloniex_endpoint)
    json_value = json.load(value)

    btc = str(json_value["USDT_BTC"]["last"])
    eth = str(json_value["USDT_ETH"]["last"])
    ltc = str(json_value["USDT_LTC"]["last"])

    crypto_pairs["btc_usdt"].append(btc)
    crypto_pairs["eth_usdt"].append(eth)
    crypto_pairs["ltc_usdt"].append(ltc)

def handle_hitbtc(crypto_pairs):
    endpoint = 'https://api.hitbtc.com/api/2/public/ticker/{}'
    btc_val = json.load(urlopen(endpoint.format('BTCUSD')))
    eth_val = json.load(urlopen(endpoint.format('ETHUSD')))
    ltc_val = json.load(urlopen(endpoint.format('LTCUSD')))

    btc = str(btc_val['last'])
    eth = str(eth_val['last'])
    ltc = str(ltc_val['last'])

    crypto_pairs["btc_usdt"].append(btc)
    crypto_pairs["eth_usdt"].append(eth)
    crypto_pairs["ltc_usdt"].append(ltc)

def handle_bittrex(crypto_pairs):
    endpoint = 'https://bittrex.com/api/v1.1/public/getticker?market={}'

    btc_val = json.load(urlopen(endpoint.format('USDT-BTC')))
    eth_val = json.load(urlopen(endpoint.format('USDT-ETH')))
    ltc_val = json.load(urlopen(endpoint.format('USDT-LTC')))

    btc = str(btc_val['result']['Last'])
    eth = str(eth_val['result']['Last'])
    ltc = str(ltc_val['result']['Last'])

    crypto_pairs["btc_usdt"].append(btc)
    crypto_pairs["eth_usdt"].append(eth)
    crypto_pairs["ltc_usdt"].append(ltc)


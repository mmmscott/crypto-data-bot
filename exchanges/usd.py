from urllib.request import urlopen
import http.client
import json

def get_all_usd_data(crypto_pairs, jpy_conversion, krw_conversion):
    handle_gemini(crypto_pairs)
    handle_cexio(crypto_pairs)
    handle_kraken(crypto_pairs)
    handle_bitstamp(crypto_pairs)
    handle_bitfinex(crypto_pairs)
    handle_coincheck(crypto_pairs, jpy_conversion)
    handle_bithumb(crypto_pairs, krw_conversion)

def handle_gemini(crypto_pairs):
    btc_endpoint = 'https://api.gemini.com/v1/pubticker/btcusd'
    eth_endpoint = 'https://api.gemini.com/v1/pubticker/ethusd'

    value = urlopen(btc_endpoint)
    json_value = json.load(value)
    crypto_pairs["btc_usd"].append(str(json_value["last"]))

    value = urlopen(eth_endpoint)
    json_value = json.load(value)
    crypto_pairs["eth_usd"].append(str(json_value["last"]))

def handle_kraken(crypto_pairs):
    value = urlopen('https://api.kraken.com/0/public/Ticker?pair=xbtusd,ethusd,ltcusd,usdtusd')
    json_value = json.load(value)
    btc = str(json_value['result']['XXBTZUSD']['c'][0])
    eth = str(json_value['result']['XETHZUSD']['c'][0])
    ltc = str(json_value['result']['XLTCZUSD']['c'][0])
    usdt = str(json_value['result']['USDTZUSD']['c'][0])

    crypto_pairs["btc_usd"].append(btc)
    crypto_pairs["eth_usd"].append(eth)
    crypto_pairs["ltc_usd"].append(ltc)
    crypto_pairs["usdt_usd"].append(usdt)

def handle_bitstamp(crypto_pairs):
    endpoint = 'https://www.bitstamp.net/api/v2/ticker/{}'
    btc = json.load(urlopen(endpoint.format("btcusd")))['last']
    eth = json.load(urlopen(endpoint.format("ethusd")))['last']
    ltc = json.load(urlopen(endpoint.format("ltcusd")))['last']

    crypto_pairs["btc_usd"].append(str(btc))
    crypto_pairs["eth_usd"].append(str(eth))
    crypto_pairs["ltc_usd"].append(str(ltc))

def handle_cexio(crypto_pairs):
    btc = str(cexio_request("/api/ticker/BTC/USD"))
    eth = str(cexio_request("/api/ticker/ETH/USD"))

    crypto_pairs["btc_usd"].append(btc)
    crypto_pairs["eth_usd"].append(eth)

def handle_bitfinex(crypto_pairs):
    endpoint = 'https://api.bitfinex.com/v2/tickers?symbols=tBTCUSD,tETHUSD,tLTCUSD'
    data = json.load(urlopen(endpoint))

    for pair in data:
        name = pair[0]
        value = str(pair[7])

        if "BTC" in name:
            crypto_pairs["btc_usd"].append(value)
        elif "ETH" in name:
            crypto_pairs["eth_usd"].append(value)
        elif "LTC" in name:
            crypto_pairs["ltc_usd"].append(value)

def handle_coincheck(crypto_pairs, conversion):
    endpoint = 'https://coincheck.com/api/ticker?pair={}'

    btc_data = json.load(urlopen(endpoint.format("btc_jpy")))['last']
    crypto_pairs["btc_usd"].append(str(btc_data / conversion))
    #eth_data = json.load(urlopen(endpoint.format("eth_jpy")))['last']
    #crypto_pairs["eth_usd"].append(str(eth_data / conversion))

def handle_bithumb(crypto_pairs, conversion):
    endpoint = 'https://api.bithumb.com/public/ticker/all'
    all_data = json.load(urlopen(endpoint))['data']

    btc = str(float(all_data["BTC"]["average_price"]) / conversion)
    eth = str(float(all_data["ETH"]["average_price"]) / conversion)
    ltc = str(float(all_data["LTC"]["average_price"]) / conversion)
    crypto_pairs["btc_usd"].append(btc)
    crypto_pairs["eth_usd"].append(eth)
    crypto_pairs["ltc_usd"].append(ltc)

def cexio_request(url):
    conn = http.client.HTTPSConnection("cex.io")
    headers = {
        'cache-control': "no-cache",
    }
    conn.request("GET", url, headers=headers)

    res = conn.getresponse()
    data = res.read()

    json_value = json.loads(data.decode("utf-8"))
    return json_value["last"]
from urllib.request import urlopen
from datetime import datetime
from subprocess import Popen
from exchanges import usd, usdt
import time
import tempfile
import os
import json

currency_endpoint = 'https://api.fixer.io/latest?base=USD'
usd_jpy_conversion = None
usd_krw_conversion = None
today = None

crypto_pairs = {
    "btc_usd": [],
    "eth_usd": [],
    "ltc_usd": [],
    "btc_usdt": [],
    "eth_usdt": [],
    "ltc_usdt": [],
    "usdt_usd": []
}

today_files = []
upload_retry_count = 0

def run_bot():
    path = setup_files()
    usd.get_all_usd_data(crypto_pairs, usd_jpy_conversion, usd_krw_conversion)
    usdt.get_all_usdt_data(crypto_pairs)

    for pair in crypto_pairs.keys():
        write_pair_data(pair, path)

def write_pair_data(pair, path):
    global today
    global crypto_pairs

    name = f"{today}-{pair}.csv"
    file_name = os.path.join(path, name)
    already_exists = os.path.exists(file_name)

    header_line = get_header_line(pair)
    now = datetime.now().strftime('%H:%M:%S%p')
    data_line = f"{now},{','.join(crypto_pairs[pair])}\n"

    #clear for next call
    crypto_pairs[pair] = []

    with open(file_name, 'a+') as f:
        if not already_exists:
            f.write(header_line)
        f.write(data_line)

def get_header_line(pair_name):
    if pair_name == "btc_usd":
        return "time,gemini,cex.io,kraken,bitstamp,bitfinex,coincheck,bithumb\n"
    elif pair_name == "eth_usd":
        return "time,gemini,cex.io,kraken,bitstamp,bitfinex,bithumb\n"
    elif pair_name == "ltc_usd":
        return "time,kraken,bitstamp,bitfinex,bithumb\n"
    elif pair_name == "usdt_usd":
        return "time,kraken\n"
    elif "usdt" in pair_name:
        return "time,poloniex,hitbtc,bittrex\n"

def setup_files():
    global today

    day = datetime.now().strftime('%B-%d-%Y')
    temp_path = tempfile.gettempdir()
    #today_files = [f"{today}-{pair}.csv" for pair in crypto_pairs.keys()]

    if usd_jpy_conversion is None or usd_krw_conversion is None:
        get_conversion_rates()

    # upload and update if new day
    if today is None:
        today = day
    elif today != day:
        #for file_name in today_files:
        #    upload_old_file(os.path.join(temp_path, file_name), file_name)
        today = day
        get_conversion_rates()

    return temp_path

def get_conversion_rates():
    global usd_krw_conversion
    global usd_jpy_conversion

    usd_conversion_rates = json.load(urlopen(currency_endpoint))
    usd_krw_conversion = usd_conversion_rates['rates']['KRW']
    usd_jpy_conversion = usd_conversion_rates['rates']['JPY']

def upload_old_file(file_location, file_name):
    global upload_retry_count

    p = Popen(["python", "upload.py", file_location, file_name])

    # try 5 times if the upload has failed
    if p.returncode != 0:
        upload_retry_count = upload_retry_count + 1
        if upload_retry_count < 5:
            upload_old_file(file_location, file_name)


if __name__ == '__main__':
    while True:
        time.sleep(5)
        try:
            run_bot()
        except (RuntimeError, TypeError, NameError):
            print('Ignoring error')

﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace CryptoDataBot.Exchanges
{
    public class ExchangeHelpers
    {
        public static HttpClient GetClient()
        {
            var client = new HttpClient();
            client.Timeout = new TimeSpan(0, 0, 2);
            return client;
        }
    }

    public class USD
    {
        public static async Task GetAllUsdData(ConcurrentDictionary<string, string> btcPairs, 
            ConcurrentDictionary<string, string> ethPairs, ConcurrentDictionary<string, string> ltcPairs,
            ConcurrentDictionary<string, string> usdtPairs, double? jpyConversion, double? krwConversion)
        {
            var geminiTask = HandleGemini(btcPairs, ethPairs);
            var cexioTask = HandleCexio(btcPairs, ethPairs);
            var krakenTask = HandleKraken(btcPairs, ethPairs, ltcPairs, usdtPairs);
            var bitstampTask = HandleBitstamp(btcPairs, ethPairs, ltcPairs);
            var bitfinexTask = HandleBitfinex(btcPairs, ethPairs, ltcPairs);
            var coincheckTask = HandleCoincheck(btcPairs, jpyConversion);
            var bithumbTask = HandleBithumb(btcPairs, ethPairs, ltcPairs, krwConversion);

            Task.WaitAll(geminiTask, cexioTask, krakenTask, bitstampTask, bitfinexTask, coincheckTask, bithumbTask);
        }

        private static async Task HandleGemini(ConcurrentDictionary<string, string> btc, ConcurrentDictionary<string, string> eth)
        {
            var btcEndpoint = "https://api.gemini.com/v1/pubticker/btcusd";
            var ethEndpoint = "https://api.gemini.com/v1/pubticker/ethusd";

            var client = ExchangeHelpers.GetClient();
            var btcTask = client.GetStringAsync(btcEndpoint);
            var ethTask = client.GetStringAsync(ethEndpoint);

            var btcString = await btcTask;
            var ethString = await ethTask;

            var btcJson = JObject.Parse(btcString);
            var ethJson = JObject.Parse(ethString);

            btc["gemini"] = btcJson["last"].ToString();
            eth["gemini"] = ethJson["last"].ToString();
        }

        private static async Task HandleCexio(ConcurrentDictionary<string, string> btc, ConcurrentDictionary<string, string> eth)
        {
            var btcEndpoint = "https://cex.io/api/ticker/BTC/USD";
            var ethEndpoint = "https://cex.io/api/ticker/ETH/USD";

            var client = ExchangeHelpers.GetClient();
            var btcTask = client.GetStringAsync(btcEndpoint);
            var ethTask = client.GetStringAsync(ethEndpoint);

            var btcString = await btcTask;
            var ethString = await ethTask;

            var btcJson = JObject.Parse(btcString);
            var ethJson = JObject.Parse(ethString);

            btc["cex.io"] = btcJson["last"].ToString();
            eth["cex.io"] = ethJson["last"].ToString();
        }

        private static async Task HandleKraken(ConcurrentDictionary<string, string> btc, ConcurrentDictionary<string, string> eth,
            ConcurrentDictionary<string, string> ltc, ConcurrentDictionary<string, string> usdt)
        {
            var endpoint = "https://api.kraken.com/0/public/Ticker?pair=xbtusd,ethusd,ltcusd,usdtusd";
            var client = ExchangeHelpers.GetClient();
            var taskString = await client.GetStringAsync(endpoint);
            var json = JObject.Parse(taskString);

            var btcValue = json["result"]["XXBTZUSD"]["c"][0].ToString();
            var ethValue = json["result"]["XETHZUSD"]["c"][0].ToString();
            var ltcValue = json["result"]["XLTCZUSD"]["c"][0].ToString();
            var usdtValue = json["result"]["USDTZUSD"]["c"][0].ToString();

            btc["kraken"] = btcValue;
            eth["kraken"] = ethValue;
            ltc["kraken"] = ltcValue;
            usdt["kraken"] = usdtValue;
        }

        private static async Task HandleBitstamp(ConcurrentDictionary<string, string> btc, ConcurrentDictionary<string, string> eth, ConcurrentDictionary<string, string> ltc)
        {
            var endpoint = "https://www.bitstamp.net/api/v2/ticker/";

            var client = ExchangeHelpers.GetClient();
            var btcTask = client.GetStringAsync($"{endpoint}btcusd");
            var ethTask = client.GetStringAsync($"{endpoint}ethusd");
            var ltcTask = client.GetStringAsync($"{endpoint}ltcusd");

            var btcString = await btcTask;
            var ethString = await ethTask;
            var ltcString = await ltcTask;

            var btcJson = JObject.Parse(btcString);
            var ethJson = JObject.Parse(ethString);
            var ltcJson = JObject.Parse(ltcString);

            btc["bitstamp"] = btcJson["last"].ToString();
            eth["bitstamp"] = ethJson["last"].ToString();
            ltc["bitstamp"] = ltcJson["last"].ToString();
        }

        private static async Task HandleBitfinex(ConcurrentDictionary<string, string> btc, ConcurrentDictionary<string, string> eth, ConcurrentDictionary<string, string> ltc)
        {
            var endpoint = "https://api.bitfinex.com/v2/tickers?symbols=tBTCUSD,tETHUSD,tLTCUSD";
            var client = ExchangeHelpers.GetClient();
            var task = client.GetStringAsync(endpoint);
            var json = JArray.Parse(await task);

            for (var i = 0; i < 3; i++)
            {
                json[i].ToObject<List<string>>();
            }
            foreach (var item in json)
            {
                var list = item.ToObject<List<string>>();
                var name = list[0];
                var value = list[7];

                if (name.Contains("BTC"))
                {
                    btc["bitfinex"] = value;
                }
                else if (name.Contains("ETH"))
                {
                    eth["bitfinex"] = value;
                }
                else if (name.Contains("LTC"))
                {
                    ltc["bitfinex"] = value;
                }

            }
        }

        private static async Task HandleCoincheck(ConcurrentDictionary<string, string> btc, double? conversion)
        {
            var endpoint = "https://coincheck.com/api/ticker?pair=btc_jpy";
            var client = ExchangeHelpers.GetClient();
            var task = client.GetStringAsync(endpoint);
            var json = JObject.Parse(await task);

            if (double.TryParse(json["last"].ToString(), out double result))
            {
                btc["coincheck"] = (result / conversion).ToString();
            }
        }

        private static async Task HandleBithumb(ConcurrentDictionary<string, string> btc, ConcurrentDictionary<string, string> eth, ConcurrentDictionary<string, string> ltc, double? krwConversion)
        {
            var endpoint = "https://api.bithumb.com/public/ticker/all";

            var client = ExchangeHelpers.GetClient();
            var task = client.GetStringAsync(endpoint);
            var json = JObject.Parse(await task);

            var data = json["data"];

            double btcVal;
            double ethVal;
            double ltcVal;

            if (Double.TryParse(data["BTC"]["average_price"].ToString(), out btcVal) &&
                Double.TryParse(data["ETH"]["average_price"].ToString(), out ethVal) &&
                Double.TryParse(data["LTC"]["average_price"].ToString(), out ltcVal))
            {
                btc["bithumb"] = (btcVal / krwConversion).ToString();
                eth["bithumb"] = (ethVal / krwConversion).ToString();
                ltc["bithumb"] = (ltcVal / krwConversion).ToString();
            }
        }
    }

    public class USDT
    {
        public static async Task GetAllUsdtData(ConcurrentDictionary<string, string> btcPairs,
            ConcurrentDictionary<string, string> ethPairs, ConcurrentDictionary<string, string> ltcPairs)
        {
            var poloniexTask = HandlePoloniex(btcPairs, ethPairs, ltcPairs);
            var hitbtcTask = HandleHitbtc(btcPairs, ethPairs, ltcPairs);
            var bittrexTask = HandleBittrex(btcPairs, ethPairs, ltcPairs);

            Task.WaitAll(poloniexTask, hitbtcTask, bittrexTask);
        }

        private static async Task HandlePoloniex(ConcurrentDictionary<string, string> btc,
            ConcurrentDictionary<string, string> eth, ConcurrentDictionary<string, string> ltc)
        {
            var endpoint = "https://poloniex.com/public?command=returnTicker";
            var client = ExchangeHelpers.GetClient();
            var stringTask = await client.GetStringAsync(endpoint);
            var json = JObject.Parse(stringTask);

            btc["poloniex"] = json["USDT_BTC"]["last"].ToString();
            eth["poloniex"] = json["USDT_ETH"]["last"].ToString();
            ltc["poloniex"] = json["USDT_LTC"]["last"].ToString();
        }

        private static async Task HandleHitbtc(ConcurrentDictionary<string, string> btc,
            ConcurrentDictionary<string, string> eth, ConcurrentDictionary<string, string> ltc)
        {
            var endpoint = "https://api.hitbtc.com/api/2/public/ticker/";

            var client = ExchangeHelpers.GetClient();
            var btcTask = client.GetStringAsync($"{endpoint}BTCUSD");
            var ethTask = client.GetStringAsync($"{endpoint}ETHUSD");
            var ltcTask = client.GetStringAsync($"{endpoint}LTCUSD");

            var btcString = await btcTask;
            var ethString = await ethTask;
            var ltcString = await ltcTask;

            var btcJson = JObject.Parse(btcString);
            var ethJson = JObject.Parse(ethString);
            var ltcJson = JObject.Parse(ltcString);

            btc["hitbtc"] = btcJson["last"].ToString();
            eth["hitbtc"] = ethJson["last"].ToString();
            ltc["hitbtc"] = ltcJson["last"].ToString();
        }

        private static async Task HandleBittrex(ConcurrentDictionary<string, string> btc,
            ConcurrentDictionary<string, string> eth, ConcurrentDictionary<string, string> ltc)
        {
            var endpoint = "https://bittrex.com/api/v1.1/public/getticker?market=";

            var client = ExchangeHelpers.GetClient();
            var btcTask = client.GetStringAsync($"{endpoint}USDT-BTC");
            var ethTask = client.GetStringAsync($"{endpoint}USDT-ETH");
            var ltcTask = client.GetStringAsync($"{endpoint}USDT-LTC");

            var btcString = await btcTask;
            var ethString = await ethTask;
            var ltcString = await ltcTask;

            var btcJson = JObject.Parse(btcString);
            var ethJson = JObject.Parse(ethString);
            var ltcJson = JObject.Parse(ltcString);

            btc["bittrex"] = btcJson["result"]["Last"].ToString();
            eth["bittrex"] = ethJson["result"]["Last"].ToString();
            ltc["bittrex"] = ltcJson["result"]["Last"].ToString();
        }
    }
}

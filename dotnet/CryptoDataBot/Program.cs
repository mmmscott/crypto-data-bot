﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using Newtonsoft.Json.Linq;
using CryptoDataBot.Exchanges;

namespace CryptoDataBot
{
    public class Program
    {
        public static string CURRENCY_ENDPOINT = "https://api.fixer.io/latest?base=USD";

        public static double? JpyConversion;
        public static double? KrwConversion;
        public static string Today { get; set; }

        public static ConcurrentDictionary<string, string> BtcUsdPairs =
             new ConcurrentDictionary<string, string>(
                 new List<KeyValuePair<string, string>>
                 {
                    new KeyValuePair<string, string>("gemini", null),
                    new KeyValuePair<string, string>("cex.io", null),
                    new KeyValuePair<string, string>("kraken", null),
                    new KeyValuePair<string, string>("bitstamp", null),
                    new KeyValuePair<string, string>("bitfinex", null),
                    new KeyValuePair<string, string>("coincheck", null),
                    new KeyValuePair<string, string>("bithumb", null)
                 }
             );

       public static ConcurrentDictionary<string, string> EthUsdPairs =
             new ConcurrentDictionary<string, string>(
                 new List<KeyValuePair<string, string>>
                 {
                            new KeyValuePair<string, string>("gemini", null),
                            new KeyValuePair<string, string>("cex.io", null),
                            new KeyValuePair<string, string>("kraken", null),
                            new KeyValuePair<string, string>("bitstamp", null),
                            new KeyValuePair<string, string>("bitfinex", null),
                            new KeyValuePair<string, string>("bithumb", null)
                 }
            );

        public static ConcurrentDictionary<string, string> LtcUsdPairs =
              new ConcurrentDictionary<string, string>(
                  new List<KeyValuePair<string, string>>
                  {
                      new KeyValuePair<string, string>("kraken", null),
                      new KeyValuePair<string, string>("bitstamp", null),
                      new KeyValuePair<string, string>("bitfinex", null),
                      new KeyValuePair<string, string>("bithumb", null)
                  }
             );

        public static ConcurrentDictionary<string, string> UsdtUsdPairs =
              new ConcurrentDictionary<string, string>(
                  new List<KeyValuePair<string, string>>
                  {
                      new KeyValuePair<string, string>("kraken", null)
                  }
             );

        public static Func<ConcurrentDictionary<string, string>> SetupUsdtPairs = () => {
            return new ConcurrentDictionary<string, string>(
                  new List<KeyValuePair<string, string>>
                  {
                      new KeyValuePair<string, string>("poloniex", null),
                      new KeyValuePair<string, string>("bitstamp", null),
                      new KeyValuePair<string, string>("bitfinex", null)
                  }
             );
        };

        public static ConcurrentDictionary<string, string> BtcUsdtPairs = SetupUsdtPairs();
        public static ConcurrentDictionary<string, string> EthUsdtPairs = SetupUsdtPairs();
        public static ConcurrentDictionary<string, string> LtcUsdtPairs = SetupUsdtPairs();

        public static Dictionary<string, ConcurrentDictionary<string, string>> FilePairs =
            new Dictionary<string, ConcurrentDictionary<string, string>>
            {
                        { "btc_usd", BtcUsdPairs },
                        { "eth_usd", EthUsdPairs },
                        { "ltc_usd", LtcUsdPairs },
                        { "btc_usdt", BtcUsdtPairs },
                        { "eth_usdt", EthUsdtPairs },
                        { "ltc_usdt", LtcUsdtPairs },
                        { "usdt_usd", UsdtUsdPairs }
            };

        public static void Main(string[] args)
        {
            while (true)
            {
                Task.Run(async () =>
                {
                    try
                    {
                        await RunBot();
                        System.Threading.Thread.Sleep(2000);
                    }
                    catch (Exception e)
                    {
                        if (!(e is TaskCanceledException))
                        {
                            Console.WriteLine(e);
                        }
                    }
                }).GetAwaiter().GetResult();
            }
        }

        private static async Task RunBot()
        {
            //Stopwatch stopwatch = Stopwatch.StartNew(); //creates and start the instance of Stopwatch

            var path = await SetupFiles();

            await USD.GetAllUsdData(BtcUsdPairs, EthUsdPairs, LtcUsdPairs, UsdtUsdPairs, JpyConversion, KrwConversion);
            await USDT.GetAllUsdtData(BtcUsdtPairs, EthUsdtPairs, LtcUsdtPairs);

            var taskList = new List<Task>();
            foreach (var pair in FilePairs)
            {
                taskList.Add(WritePairData(pair, path));
            }
            Task.WaitAll(taskList.ToArray());

            //stopwatch.Stop();
            //Console.WriteLine(stopwatch.ElapsedMilliseconds);
        }

        private static async Task WritePairData(KeyValuePair<string, ConcurrentDictionary<string, string>> pair, string path)
        {
            var name = $"{Today}-{pair.Key}.csv";
            var fileName = Path.Combine(path, name);
            var exists = File.Exists(fileName);

            var headerLine = GetHeaderLine(pair.Key);
            var now = DateTime.Now.ToString("hh:mm:sstt");
            var writeLine = $"{now},{GenerateDataLine(pair.Value, pair.Key)}";

            if (!exists)
            {
                File.AppendAllText(fileName, headerLine);
            }

            File.AppendAllText(fileName, writeLine);
        }

        private static string GenerateDataLine(ConcurrentDictionary<string, string> pair, string pairName)
        {
            switch (pairName)
            {
                case "btc_usd":
                    return $"{pair["gemini"]},{pair["cex.io"]},{pair["kraken"]},{pair["bitstamp"]},{pair["bitfinex"]},{pair["coincheck"]},{pair["bithumb"]}{Environment.NewLine}";
                case "eth_usd":
                    return $"{pair["gemini"]},{pair["cex.io"]},{pair["kraken"]},{pair["bitstamp"]},{pair["bitfinex"]},{pair["bithumb"]}{Environment.NewLine}";
                case "ltc_usd":
                    return $"{pair["kraken"]},{pair["bitstamp"]},{pair["bitfinex"]},{pair["bithumb"]}{Environment.NewLine}";
                case "usdt_usd":
                    return $"{pair["kraken"]}{Environment.NewLine}";
                default:
                    return $"{pair["poloniex"]},{pair["hitbtc"]},{pair["bittrex"]}{Environment.NewLine}";
            }
        }
        private static string GetHeaderLine(string pairName)
        {
            switch (pairName)
            {
                case "btc_usd":
                    return $"time,gemini,cex.io,kraken,bitstamp,bitfinex,coincheck,bithumb{Environment.NewLine}";
                case "eth_usd":
                    return $"time,gemini,cex.io,kraken,bitstamp,bitfinex,bithumb{Environment.NewLine}";
                case "ltc_usd":
                    return $"time,kraken,bitstamp,bitfinex,bithumb{Environment.NewLine}";
                case "usdt_usd":
                    return $"time,kraken{Environment.NewLine}";
                default:
                    return $"time,poloniex,hitbtc,bittrex{Environment.NewLine}";
            }
        }

        private static async Task<string> SetupFiles()
        {
            var day = DateTime.Now.ToString("MMMM-dd-yyyy");
            var tempPath = Path.GetTempPath();

            if (JpyConversion == null || KrwConversion == null) {
                await GetCurrencyConversionRates();
            }

            if (Today == null)
            {
                Today = day;
            }
            else if (Today != day)
            {
                Today = day;
                await GetCurrencyConversionRates();
            }

            return tempPath;
        }

        private static async Task GetCurrencyConversionRates()
        {
            var client = new HttpClient();
            var stringTask = client.GetStringAsync(CURRENCY_ENDPOINT);
            var json = await stringTask;
            var rates = JObject.Parse(json);

            JpyConversion = double.Parse(rates["rates"]["JPY"].ToString());
            KrwConversion = double.Parse(rates["rates"]["KRW"].ToString());
        }
    }
}
